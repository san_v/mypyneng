#!python3

london_co = {
'r1' : {
'location': '21 New Globe Walk',
'vendor': 'Cisco',
'model': '4451',
'ios': '15.4',
'ip': '10.255.0.1'
},
'r2' : {
'location': '21 New Globe Walk',
'vendor': 'Cisco',
'model': '4451',
'ios': '15.4',
'ip': '10.255.0.2'
},
'sw1' : {
'location': '21 New Globe Walk',
'vendor': 'Cisco',
'model': '3850',
'ios': '3.6.XE',
'ip': '10.255.0.101',
'vlans': '10,20,30',
'routing': True
}
}

dev_name = input("Enter device name: ")
device_subdict = london_co.get(dev_name)
dev_parameter = input("Enter parameter name ({}): ".format(','.join(device_subdict.keys())))
print (london_co.get(dev_name).get(dev_parameter,'No such parameter'))
