#!python3

access_template = ['switchport mode access',
'switchport access vlan {}',
'switchport nonegotiate',
'spanning-tree portfast',
'spanning-tree bpduguard enable']

trunk_template = ['switchport trunk encapsulation dot1q',
'switchport mode trunk',
'switchport trunk allowed vlan {}']

#Create dictionary with required templates
config_templates = {
'access' : access_template,
'trunk'  : trunk_template
}

intf_mode = input('Enter interface mode (access/trunk): ')
intf_name = input('Enter interface type and number (example Gi0/3): ')
intf_vlan = input('Enter vlan(s): ')

print ('\ninterface {}'.format(intf_name))
print ('\n'.join(config_templates.get(intf_mode)).format(intf_vlan))
