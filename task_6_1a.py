#!python3
import sys

in_addr_str = input('Enter IP address in format x.x.x.x: ')
addr_octets = in_addr_str.split('.')

#Create flag
is_valid_address = True

#Check number of octets
is_valid_address = is_valid_address and len(addr_octets) == 4
addr_octets_int = []

#Check each octet to be valid number in range [0..255]
for octet in addr_octets:
    is_valid_address = is_valid_address and octet.isdigit()
    if is_valid_address:
        addr_octets_int.append(int(octet))
        if addr_octets_int[-1] < 0 or addr_octets_int[-1] > 255:
            is_valid_address = False
    else: break
    
if is_valid_address is not True:
    print('Incorrect IPv4 address')
    sys.exit()


addr_int32 = 0
for i in range(4):
    addr_int32 += (addr_octets_int[i]<<((3-i)*8))


if addr_int32 == 0:
    print("unassigned")
elif addr_int32 > 0 and addr_int32 < (224<<24):
    print("unicast")
elif addr_int32 < (240<<24):
    print("multicast")
elif addr_int32 == 2**32-1:
    print("local broadcast")
else: print("unused")
