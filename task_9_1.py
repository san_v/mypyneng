#!python3

def generate_access_config(access):
    access_template = ['switchport mode access',
                       'switchport access vlan',
                       'switchport nonegotiate',
                       'spanning-tree portfast',
                       'spanning-tree bpduguard enable']

    result_list = []
    for port,vlan in access.items():
        result_list.append('interface {}'.format(port))
        for command in access_template:
            result_list.append(command)
            if command.endswith('access vlan'):
                result_list[-1] += ' {}'.format(vlan) #adding vlan number to the end of last appended element
    return result_list
    

access_dict = {'FastEthernet0/12':10,
             'FastEthernet0/14':11,
             'FastEthernet0/16':17,
             'FastEthernet0/17':150}

print(generate_access_config(access_dict))