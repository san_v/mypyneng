#!python3

def generate_trunk_config(trunk):

    trunk_template = ['switchport trunk encapsulation dot1q',
                      'switchport mode trunk',
                      'switchport trunk native vlan 999',
                      'switchport trunk allowed vlan']
    result_lst = []
    for interface, vlan_list in trunk.items():
        result_lst += ['interface {}'.format(interface)]
        result_lst += trunk_template
        result_lst[-1] += ' {}'.format(str(vlan_list).strip('[]').replace(' ',''))
    return result_lst
    


trunk_dict = { 'FastEthernet0/1':[10,20,30],
               'FastEthernet0/2':[11,30],
               'FastEthernet0/4':[17] }

print(generate_trunk_config(trunk_dict))