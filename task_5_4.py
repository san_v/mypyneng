#!python3

num_list = [10, 2, 30, 100, 10, 50, 11, 30, 15, 7]
word_list = ['python', 'ruby', 'perl', 'ruby', 'perl', 'python', 'ruby', 'perl']

input_num = int(input('Enter number from the list {}: '.format(num_list)))

#Create a list of all indexes for entered number
num_indexes = [i for i in range(len(num_list)) if num_list[i] == input_num]
#Print the last element of the indexes list
print ('The highes index for the number is: {}'.format(num_indexes[-1:]))

input_word = input('Enter word from the list {}: '.format(word_list))
word_indexes = [i for i in range(len(word_list)) if word_list[i] == input_word]
print ('The highes index for the word is: {}'.format(word_indexes[-1:]))