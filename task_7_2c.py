#!python3

# Filename is './07_files/config_sw1.txt' - input
# Filename is './07_files/config_sw1_cleared.txt' - output

from sys import argv

ignore = ['duplex', 'alias', 'Current configuration']
result = []

with open(argv[1]) as file, open(argv[2],'w') as output:
    for line in file:
        for ignore_word in ignore:
            if ignore_word in line: break
        else: result.append(line)
    output.writelines(''.join(result))