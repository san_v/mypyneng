#!python3

in_addr_str = input('Enter IP address in format x.x.x.x: ')
addr_octets = in_addr_str.split('.')
addr_octets_int = [int(addr_octets[i]) for i in range(len(addr_octets))]

addr_int32 = 0
for i in range(4):
    addr_int32 += (addr_octets_int[i]<<((3-i)*8))


if addr_int32 == 0:
    print("unassigned")
elif addr_int32 > 0 and addr_int32 < (224<<24):
    print("unicast")
elif addr_int32 < (240<<24):
    print("multicast")
elif addr_int32 == 2**32-1:
    print("local broadcast")
else: print("unused")
