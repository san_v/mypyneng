#!python3
from sys import argv

user_info = argv[1]

#Parsing user info
#IP address parsing
ip_addr_str_lst = user_info[:-3].split('.')
ip_addr_int_lst = [int(octet) for octet in ip_addr_str_lst]

# Convert IP address to 32-bit integer
ip_addr = ip_addr_int_lst[0]*256**3 + \
          ip_addr_int_lst[1]*256**2 + \
          ip_addr_int_lst[2]*256**1 + \
          ip_addr_int_lst[3] 


#Netmask parsing
prefix_len = int(user_info[-2:]) #Strip prefix length from the string
mask_int = (2**32-1) - (2**(32-prefix_len) - 1) # Convert prefix length to netmask 32-bit integer value
mask_int_lst = [((mask_int>>i*8)&255) for i in range(3,-1,-1)] # Generate list of netmask octets

#Perform masking to get subnet address from IP address (32-bit int representation)
subnet_int = ip_addr & mask_int
subnet_int_lst = [((subnet_int>>i*8)&255) for i in range(3,-1,-1)] # Generate list of subnet octets

#Output
dec_output_template = "{:<10}{:<10}{:<10}{:<10}"
bin_output_template = "{:08b}  {:08b}  {:08b}  {:08b}"

print("Network:")
print(dec_output_template.format(subnet_int_lst[0],subnet_int_lst[1],subnet_int_lst[2],subnet_int_lst[3]))
print(bin_output_template.format(subnet_int_lst[0],subnet_int_lst[1],subnet_int_lst[2],subnet_int_lst[3]))
print("\nMask:")
print("/{}".format(prefix_len))
print(dec_output_template.format(mask_int_lst[0],mask_int_lst[1],mask_int_lst[2],mask_int_lst[3]))
print(bin_output_template.format(mask_int_lst[0],mask_int_lst[1],mask_int_lst[2],mask_int_lst[3]))


