#!python3

print_template = '''\
Protocol:             {}
Prefix:               {}
AD/Metric:            {}
Next-Hop:             {}
Last Update:          {}
Outbound Interface:   {}
'''

file = open('./07_files/ospf.txt','r')
lines = file.read().strip()
lines = lines.replace(',','')
lines_lst = lines.split('\n')

for line in lines_lst:
    route = line.split()
    route[0] = route[0].replace('O','OSPF')
    route[2] = route[2].strip('[]')
    route.pop(3)
    print(print_template.format(*route))

#Don't forget to close file if not using "with"
file.close()