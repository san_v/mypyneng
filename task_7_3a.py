#!python3

in_filename = './07_files/CAM_table.txt'

result_line = []
with open(in_filename) as in_file:
    for line in in_file:
        split_line = line.split()
        if (len(split_line)) >= 2:
            mac_address = split_line[1].split('.')
            if len(mac_address) == 3:
                try:
                    int(''.join(mac_address),16)
                    split_line.pop(2)
                    result_line.append('    '.join(split_line))
                except ValueError: pass
result_line.sort()
print ('\n'.join(result_line))