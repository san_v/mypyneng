#!python3

def generate_access_config(access, psecurity=False):
    access_template = ['switchport mode access',
                       'switchport access vlan',
                       'switchport nonegotiate',
                       'spanning-tree portfast',
                       'spanning-tree bpduguard enable']
    
    port_security = ['switchport port-security maximum 2',
                     'switchport port-security violation restrict',
                     'switchport port-security']    

    result_dict = {}
    for port,vlan in access.items():
        temp_list = []
        for command in access_template:
            temp_list.append(command)
            if command.endswith('access vlan'):
                temp_list[-1] += ' {}'.format(vlan) #adding vlan number to the end of last appended element
        if psecurity is True:
            temp_list += port_security
        
        result_dict[port] = temp_list
        
    return result_dict
    

access_dict = {'FastEthernet0/12':10,
             'FastEthernet0/14':11,
             'FastEthernet0/16':17,
             'FastEthernet0/17':150}

print(generate_access_config(access_dict))
print(generate_access_config(access_dict,False))
print(generate_access_config(access_dict,True))
print(generate_access_config(access_dict,psecurity=True))