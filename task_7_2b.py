#!python3

# Filename is './07_files/config_sw1.txt'

from sys import argv

ignore = ['duplex', 'alias', 'Current configuration']

with open(argv[1]) as file, open('./07_files/config_sw1_cleared.txt','w') as output:
    for line in file:
        for ignore_word in ignore:
            if ignore_word in line: break
        else: output.write(line)