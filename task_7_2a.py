#!python3

# Filename is './07_files/config_sw1.txt'

from sys import argv

ignore = ['duplex', 'alias', 'Current configuration']

with open(argv[1]) as file:
    for line in file:
        if line.startswith('!') is not True:
            for ignore_word in ignore:
                if ignore_word in line: break
            else: print(line,end='')